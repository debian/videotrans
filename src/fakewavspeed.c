/*
Copyright (c) 2005-2007, Sven Berkvens-Matthijsse

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.

* Neither the name of deKattenFabriek nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include	"config.h"

#include	<stdio.h>
#include	<string.h>
#include	<errno.h>
#include	<stdlib.h>
#ifdef		HAVE_UNISTD_H
#include	<unistd.h>
#endif		/* HAVE_UNISTD_H */

/* Name:	main
 *
 * Description:	Main entrance to the program.
 *
 * Input:	argc		- the number of arguments
 *		argv		- the array of arguments
 *
 * Output:	Standard exit code.
 *
 * Side effects:None.
 */

int
main(const int argc, char * const * const argv)
{
	int		speed;
	unsigned int	uspeed;
	unsigned char	buffer[65536];
	size_t		size;

	/* Check the argument.
	 */

	if (argc != 2)
	{
		(void)fprintf(stderr, "Usage: movie-fakewavspeed speed\n");
		exit(1);
	}

	speed = atoi(argv[1]);
	if ((speed <= 0) || (speed > 1000000))
	{
		(void)fprintf(stderr, "Illegal speed %d supplied, should be "
			"between 1 and 1000000\n", speed);
		exit(1);
	}

	uspeed = (unsigned int)speed;

	/* Read 44 bytes of wave header.
	 */

	if (fread(buffer, 44, 1, stdin) != 1)
	{
		(void)fprintf(stderr, "Unable to read the WAV header: %s\n",
			strerror(errno));
		exit(1);
	}

	/* Check whether this is indeed a WAV file.
	 */

	if (('R' != buffer[0]) || ('I' != buffer[1]) ||
		('F' != buffer[2]) || ('F' != buffer[3]) ||
		('W' != buffer[8]) || ('A' != buffer[9]) ||
		('V' != buffer[10]) || ('E' != buffer[11]) ||
		('f' != buffer[12]) || ('m' != buffer[13]) ||
		('t' != buffer[14]) || (' ' != buffer[15]))
	{
		(void)fprintf(stderr, "Standard input is not a WAV file!\n");
		exit(1);
	}

	/* Patch the SampleRate field.
	 */

	buffer[24] = (unsigned char)(uspeed >> 0);
	buffer[25] = (unsigned char)(uspeed >> 8);
	buffer[26] = (unsigned char)(uspeed >> 16);
	buffer[27] = (unsigned char)(uspeed >> 24);

	/* Patch the ByteRate field.
	 */

	uspeed *= (buffer[22] | (buffer[23] << 8));	/* NumChannels */
	uspeed *= (buffer[34] | (buffer[35] << 8));	/* BitsPerSample */
	uspeed /= 8;

	buffer[28] = (unsigned char)(uspeed >> 0);
	buffer[29] = (unsigned char)(uspeed >> 8);
	buffer[30] = (unsigned char)(uspeed >> 16);
	buffer[31] = (unsigned char)(uspeed >> 24);

	/* Write out the WAV header.
	 */

	if (fwrite(buffer, 44, 1, stdout) != 1)
	{
		(void)fprintf(stderr, "Cannot write WAV header to stdout: %s\n",
			strerror(errno));
		exit(1);
	}

	/* Now, read bytes from the input and copy them to the output.
	 */

	while (1)
	{
		/* Read from the input.
		 */

		size = fread(buffer, 1, 65536, stdin);
		if (0 == size)
			break;

		/* Write what we have to the output channel.
		 */

		if (fwrite(buffer, size, 1, stdout) != 1)
		{
			(void)fprintf(stderr,
				"Cannot write WAV data to stdout: %s\n",
				strerror(errno));
			exit(1);
		}
	}

	/* Check for errors.
	 */

	if (ferror(stdin))
	{
		(void)fprintf(stderr, "Cannot read WAV data from stdin: %s",
			strerror(errno));
		exit(1);
	}

	/* Flush standard output.
	 */

	if (fflush(stdout))
	{
		(void)fprintf(stderr, "Cannot flush WAV data to stdout: %s\n",
			strerror(errno));
		exit(1);
	}

	/* We're done.
	 */

	return 0;
}
