videotrans (1.6.1-9) unstable; urgency=medium

  * QA upload.

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ David da Silva Polverari ]
  * debian/control:
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Bumped Standards-Version to 4.6.2.
      - Bumped debhelper-compat level to 13.
      - Updated Vcs-* fields to public 'debian' namespace.
  * debian/copyright: updated packaging copyright years.
  * debian/gbp.conf: added to document repository layout.
  * debian/lintian-overrides:
      - Fixed typo on comment.
      - Updated lintian override to new format.
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.
  * debian/source/lintian-overrides: created to override a false-positive
    lintian.
  * debian/upstream/metadata: created to provide DEP12 metadata about upstream.

 -- David da Silva Polverari <polverari@debian.org>  Mon, 22 Jan 2024 04:42:41 +0000

videotrans (1.6.1-8) unstable; urgency=medium

  * QA upload

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Set Vcs-* to salsa.debian.org
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove trailing whitespaces
  * d/watch: Use https protocol

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Herbert Fortes ]
  * DH_LEVEL: 11
  * Add CI test
  * d/control:
       - Remove myself from Uploaders field
       - Maintainer field: Debian QA Group
       - Bump Standards-Version from 3.9.7 to 4.2.1

 -- Herbert Parentes Fortes Neto <hpfn@debian.org>  Thu, 15 Nov 2018 08:48:17 -0200

videotrans (1.6.1-7) unstable; urgency=low

  * debian/control:
      - Maintainer: Debian Multimedia Maintainers
      - Uploaders : myself
      - Vcs* : back to pkg-multimedia

 -- Herbert Parentes Fortes Neto <hpfn@ig.com.br>  Fri, 08 Apr 2016 11:08:23 -0300

videotrans (1.6.1-6) unstable; urgency=low

  * avconv -> ffmpeg conversion.
  * debian/control:
      - Replace libav-tools with ffmpeg in the Depends field.
  * debian/copyright updated.
  * debian/lintian-overrides created.
  * debian/patches:
      - 03-ffmpeg_to_avconv.patch comment in series file. So not in use.
        File kept.
  * debian/rules:
      - hardening added.
  * debian/watch:
      - version 4.

 -- Herbert Parentes Fortes Neto <hpfn@ig.com.br>  Wed, 02 Mar 2016 13:21:34 -0300

videotrans (1.6.1-5) unstable; urgency=low

  * New maintainer. Thanks Alessio for the working done.
    (Closes: #815732)
  * debian/control:
      - set myself as a maintainer.
      - Bump Standards-Version from 3.9.4 to 3.9.7

 -- Herbert Parentes Fortes Neto <hpfn@ig.com.br>  Tue, 01 Mar 2016 15:10:33 -0300

videotrans (1.6.1-4) unstable; urgency=medium

  * QA upload.
  * Orphaning.

 -- Alessio Treglia <alessio@debian.org>  Wed, 24 Feb 2016 08:07:21 +0000

videotrans (1.6.1-3) unstable; urgency=medium

  * debian/patches/04-update_imagemagick.patch:
    - Update movie-title script to make it work with latest version of
      ImageMagick. (Closes: #778866)

 -- Alessio Treglia <alessio@debian.org>  Tue, 24 Feb 2015 11:04:26 +0000

videotrans (1.6.1-2) unstable; urgency=low

  * ffmpeg -> avconv conversion.
  * Replace ffmpeg with avconv in the Depends field (Closes: #721162)
  * Bump Standards.

 -- Alessio Treglia <alessio@debian.org>  Tue, 01 Oct 2013 18:19:13 +0100

videotrans (1.6.1-1) unstable; urgency=low

  * Initial release. (Closes: #434652)

 -- Alessio Treglia <alessio@debian.org>  Thu, 11 Oct 2012 00:39:05 +0100
