CHANGES FOR VERSION 1.6.1
=========================

* Minor cleanups.

- Fixed the usage of minus signs and hyphens in the manual pages. Thanks
  to Sikon for supplying a patch file!

- Fixed the installation of library.sh, so that it is installed without
  execute permissions. The file is not intended to be run as a
  standalone executable.

- Added a hint for a certain error condition.

- Fixed compilation on Ubuntu Oneiric.

- Removed movie-rip-tv.com, which does not work any longer.



CHANGES FOR VERSION 1.6.0
=========================

* Cleanup for the entire package. Minor feature updates.

- A bug in the audio detection caused some movies to be regarding as if
  they had no audio in them. This is now fixed.

- The movie-compare-dvd program has been fixed to use one of three
  generally available programs to calculate checksums with (instead
  of just assuming that the "md5" program is available), and now
  checks whether the directories listed on the command line are
  actually present and are indeed directories.

- The documentation on subtitles in the manual page for movie-to-dvd
  has been updated to reflect the actual behaviour of movie-to-dvd
  with regard to subtitling.

- The movie-to-dvd program now has the option -Q which allows the user
  to override the audio bitrate in the resulting audio file.

- Removed the "widescreen" attribute in the produced dvdauthor XML
  output file, which the newest dvdauthor does not support for 4:3
  source material.

- Added the -nojoystick and -nolirc arguments to mplayer invocations,
  so that peripheral devices do not affect the encoding process.

- Added the COPYING file that explains the license for Videotrans,
  which is required by some Linux distributions such as Ubuntu.

- Added the licensing and copyright information to all the shell
  scripts and C programs.

- People making binary distributions can now use the standard
  DESTDIR environment variable to install Videotrans in a different
  location than it will be eventually installed.



CHANGES FOR VERSION 1.5.3
=========================

* Minor bugfix for audio parameter detection.

- A bug fixed in the function library that detects audio and video
  parameters, that caused the audiorate of a given audio to be wrong
  sometimes. The problem was that mplayer sometimes repeats the
  ID_AUDIO_BITRATE line with a different and wrong value the second time.

- ffmpeg cannot resample from 5 to 6 channels when encoding AC3 files,
  and mplayer cannot do this conversion either. So videotrans now uses
  the number of channels from the source, if this number is greater than
  2.

- Changed parameters for ffmpeg (added the 'k' in the -ab option).

- Fixed the determination of the number of audio channels in the input.
  The ID_AUDIO_NCH line was sometimes present more than once, with
  incorrect values.



CHANGES FOR VERSION 1.5.2
=========================

* Important bugfix that solves the problem where spumux could crash
  while a DVD menu was being created.

- A bug was fixed in movie-title where movie-title would call ImageMagick's
  convert with wrong parameters, producing a defective PNG which would cause
  spumux (in the dvdauthor package) to crash later on in the building
  process. Whether this would happen or not seems fairly random but was a
  big nuisance that affected a lot of users.

- movie-rip-tv.com now works again with tv.com's new URL format.



CHANGES FOR VERSION 1.5.1
=========================

* Bugfix for movie-title regarding the newest version of ImageMagick where
  the background and movie preview images would not be visible in the menu.
  Bugfix for movie-rip-tv.com where an episode's summary would sometimes not
  be grabbed.

- There was a bug in movie-rip-tv.com where the summary of an episode would
  not be grabbed if there was a movie icon next to the summary.

- The newest version of ImageMagick does not work in the way that other
  versions of ImageMagick worked regarding the transparent color of the
  overlay image used in the DVD menu generation in the movie-title program.
  This has now been changed to use PNG instead of GIF, and should work in
  all versions of ImageMagick.



CHANGES FOR VERSION 1.5.0
=========================

* There was a bug in the Makefiles causing some systems to refuse to
  install the package. The tv.com ripper now support tv.com's new layout.
  The movie-title program now renders sensible menus if no preview images
  are used. The movie-to-dvd program now supports the forcing of a specific
  audio codec while generating the destination video's audio. Movies without
  audio are now handled correctly.

- Fixed the Makefiles in the src and data directories to install scripts
  using the INSTALL_SCRIPT macro instead of the INSTALL_PROGRAM macro,
  which tries to strip binaries on some systems, which causes the install
  process to botch on scripts installed this way.

- movie-title now supports grid sizes larger than 9 by 9. 

- movie-title now renders sensible menus if no preview images are used.
  This allows one to put up to 48 titles into the menu.

- movie-rip-tv.com now supports the new layout that tv.com has adopted.

- movie-to-dvd now has an option -c that allows the user to choose which
  audio format is to be used in the generated VOB. The default has been
  changed to AC3 for all types of audio channels.

- movie-to-dvd now handles movies without audio correctly by creating silence
  of the correct length in the destination audio stream.

- movie-to-dvd now adds mplayer's "volnorm" audio filter into the audio filter
  chain to make sure that the entire audio bandwidth is used.



CHANGES FOR VERSION 1.4.1
=========================

* There was a bug in the program that caused a not-much-used option to
  movie-to-dvd to be ignored instead of honored.

- Fixed movie-zoomcalc so that a specified destination resolution will
  override whatever -choosedvd will determine for you.



CHANGES FOR VERSION 1.4.0
=========================

* There were some issues with the aspects of images in the title sequences.
  Furthermore, panscan images were hardly ever correct regarding their
  aspects.

- Fixed movie-title so that images in the preview boxes are now correct
  regarding their aspects.

- Modified the aspects.txt documentation file so that it now reflects the
  proper way of dealing with different aspects, zooming, letterboxing and
  panscanning.

- Fixed movie-zoomcalc so that panscanning now works correctly.



CHANGES FOR VERSION 1.3.1
=========================

* Fixed a number of minor bugs that caused some trouble on some platforms.

- Fixed some issues with backslashes in single quotes in backquotes. Some
  shells remove one level of quoting when finding the end of the backquotes,
  most don't.



CHANGES FOR VERSION 1.3.0
=========================

* Fixed a number of bugs and added new features for creating DVD menus. DVD
  menus now have many more options. Also, the program is now compatible with
  some versions of ImageMagick and mplayer that act in different ways than
  other versions of those programs.


- Added movie-make-title-simple, which is a program that is similar to
  movie-make-title and can create title sequence for use with movie-title,
  but now using an optional static background image and an optional audio
  file (or a combination).

- Added the -n option in movie-make-title, which allow the user to choose
  how the movies should be previewed in the menu (animated, static or not
  previewed at all).

- The movie-title program now produces chapters in each movie. The interval
  for the chapters is configurable. Creation of chapters can also be disabled.

- Added the -b and -O options for movie-to-dvd, which allows the user to tell
  mplayer how to play specific movies using specific options.

- The newest versions of ImageMagick were not working correctly with
  previous versions of Videotrans. This has been fixed so that all versions
  of ImageMagick should work fine.

- On some systems, mplayer crashed while playing mf://directory/*.jpg style
  "movies" in movie-title. I could not find the cause and I have decided
  to use mjpeg's jpeg2yuv here, which seems to work fine everywhere.

- Made the programs' usage information clearer in many cases, with a lot
  more text explaining what the options do.

- Corrected some defective URLs in the configure program (which gave the
  user a wrong location to fetch missing software from).

- All the programs now check whether the specified filenames are not going
  to confuse the program that need to process those filenames.

- Added a movie-rip-epg.data program that can convert an epg.data file that
  VDR generates into a videotrans .info file that movie-title understands.

- Updated the documentation.



CHANGES FOR VERSION 1.2.1
=========================

- Fixed a bug in movie-rip-tv.com which caused it to hang while finding the
  main URL of a series that it had no cached URL for.



CHANGES FOR VERSION 1.2.0
=========================

- Fixed a few misnomers in the documentation (historical program names were
  still being used).

- Fixed the tv.com ripper to remove the extra stuff from URLs that tv.com
  uses these days.

- Fixed movie-make-title to create WAVs with the correct number of audio
  channels instead of always creating WAVs with six channels.

- Fixed an issue with some newer version of ImageMagick's "convert", where
  drawing an image from an external file onto the working image _requires_
  quotes around the filename, where the quotes were optional in older
  versions.

- Fixed escaping issues in all the programs. They now all accept all kinds
  of characters in filenames, everything is escaped correctly.

- The movie-to-dvd program now has an option with which you can specify
  a directory in which to write the results, instead of in the same directory
  as where the source resides.

- The movie-title program now has an option with which you can specify that
  the program should not start at the beginning of the movies to capture frames
  for use in the menus, but that it should start capturing after a number of
  seconds.

- The programs now don't write stream.yuv and temporary .wav named pipes in
  the current directory (which may not be writable), but use the /tmp instead.

- A progress meter was added which tells you how long it will take for an
  encode to finish and how much progress has been made (percentage).

- Fixed uses of lynx and fmt that we not supported correctly on all platforms.

- Fixed the movie-title manual page to clarify on the handling of movies that
  have been split up into multiple sections.



CHANGES FOR VERSION 1.1.0
=========================

- Fixed a problem where 5.1 AC3 audio would not be correctly transcoded if
  the audio pitch had to be adjusted. This was due to sox, an audio
  manipulation package that videotrans used, that can only handle up to four
  channels in a WAV file. The problem has been fixed by asking mplayer to
  up- or downsample the audio and then using a new program movie-fakewavspeed
  to fake the samplerate back to 48000 Hz. This effectively produces the same
  result, but now without sox.

- Fixed another bug in the data/Makefile.in where the mkdir would fail if the
  videotrans directory already existed.

- Fixed some documentation bugs and omissions.

- Added documentation for the movie-rip-tv.com program.

- Cleaned up a lot of code to be shorter and more legible.

- movie-title and movie-make-title now accept audio with more than two
  channels.

- Made all the program more robust regarding errors and more clean in their
  output.



CHANGES FOR VERSION 1.0.1
=========================

- Fixed an installation problem (the "videotrans" directory was not created
  in the share directory).



CHANGES FOR VERSION 1.0.0
=========================

- Completely cleaned up the entire source tree. Everything is now put neatly
  into separate directories.

- Fixed quite a few bugs.

- Added some enhancements, for example, AC3 surround sound.

