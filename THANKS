I'd like to thank the following people for their feedback on this program:

Peter Cranmer
	The first person ever to mail me about Videotrans :-) He had a few
	installation issues with the first versions of Videotrans and also
	had a problem with encoding AC3 5.1 movies from NTSC to PAL, which
	he reported and I fixed by eliminating the use of "sox", which can't
	handle six channels of sound (four is the maximum).

Steve Head
	Reported problems with the stream.yuv named pipe being created in
	the current directory, which was not always possible due to
	permissions and such. I've fixed this issue by moving all temporary
	named pipes to the /tmp directory. Steve also requested the
	possibility to write the output of an encoding session into another
	directory than the directory in which the source was present. I've
	implemented this feature. Steve also reported some problems with long
	filenames where the last part of that filename would be garbled in some
	situations. That issue boiled down to be "bash"'s fault, which has
	already been fixed in the newest versions of "bash".

Pierre Larroque
	Reported problems with ImageMagick's "convert". He used a newer
	version than I did, and his version was more restrictive regarding
	the filenames that were used in the -draw option. I've fixed this so
	that it works correctly in the older and newer versions of "convert".

Duncan McQueen
	Reported problems with movie-rip-tv.com, which was out-of-date since
	tv.com changed its URL structure. I've fixed this.


Glen Stewart
	Reported that not every filename he used worked correctly. I've fixed
	this issue by escaping all the strings used anywhere correctly.

Wes Hertlein
	For pointing out some unclear passages in the manual pages (which
	have been altered to make them clearer). Also, he pointed out some
	uses of lynx and fmt that do not work on every platform. These
	problems have also been fixed.

Bram Avontuur
	For pointing out that the URL of the MJPEG tools project is not
	http://mjpegtools.sourceforge.net/ but http://mjpeg.sourceforge.net/
	and that configure.ac did not contain a test for spumux, which is
	part of the dvdauthor package and is required by movie-title.

Gr�goire Favre
	For making suggestions that led to the creation of the
	movie-make-title-simple program and the adjustments to movie-title
	that were required to support simple menus. Furthermore, we worked
	extensively together to solve multiple bugs.

Paul Smith
	For pointing out that some AVI file need mplayer's -nobps option to
	play correctly. I added the -b option to movie-to-dvd for this purpose.
	Furthermore, I added the -O option as well so that people can supply
	their own options to mplayer if they need to.

	Also, Paul pointed out that the audio level of an encoded movie is not
	equal to the source's audio level. I've tried to circumvent this by
	using mplayer's "volnorm" audio filter.

Stephen Lee
	For pointing out that according to the DVD standard, PAL DVD players
	are required to support MP2 and AC3, and NTSC DVD players are
	required to support AC3 but not MP2. I've changed the audio routines
	to take this into account. Further, movie-to-dvd's -c option was born
	because of this, allowing the forcing of a specific audio codec.

mjlar94 (real name unknown)
	For pointing out that mplayer's keyboard controls were still active
	while transcoding, which causes problems if keystrokes are accidentally
	typed in the window in which Videotrans is running. I've disabled the
	keyboard controls now in all situations where mplayer is used.

Alexandre Bourget
	For pointing out that movie-to-dvd did not handle movies without
	audio correctly. I've fixed this by making mplayer read /dev/zero
	as a fake source of audio, causing it to create silence of the correct
	length, which will then be placed into the destination audio stream.

Maarten Boekhold
	For asking for the audio bitrate overriding option, which is now
	implemented in movie-to-dvd in the form of the -Q option.

S�rgio Monteiro Basto
	For reporting that the newest dvdauthor does not support the
	"widescreen" attribute for 4:3 source material.

"Multinymous"
	For reporting that -nojoystick and -nolirc are necessary options
	to mplayer to avoid corruption of the encoding.

Sikon
	For supplying a patch file to fix the usage of minus signs and
	hyphens in the manual pages. Furthermore, he pointed out that
	library.sh should not be installed as an executable script, but
	as a non-executable text file (it is never run standalone).
